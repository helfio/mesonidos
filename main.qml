/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.14
import QtQuick.Controls 2.14
import jpns.reproductor 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    property bool dev: false
    title: qsTr("Mesonidos")
//    color: "#008bff"

    header: Item{
        height: 60
        width: parent.width

        Rectangle{
            color: "transparent"
            border.color: "red"
            anchors.fill: parent
            visible: dev
        }
        Row{
            height: parent.height-10
            anchors.verticalCenter: parent.verticalCenter
            spacing: 6
            leftPadding: spacing
            visible: swipeView.visible

            Button{
                id: playButton
                text: "Play"
                height: parent.height
                font.pixelSize: 15
                enabled: reproductor.fuente != "" && !reproductor.reproduciendo
                onClicked: reproductor.reproducir()
            }
            Button{
                id: stopButton
                text: "Stop"
                height: parent.height
                font.pixelSize: 15
                enabled: reproductor.reproduciendo
                onClicked: reproductor.stop()
            }
            Button{
                id: scanButton
                text: "Escanear"
                height: parent.height
                font.pixelSize: 15
                onClicked: gestorSonidos.escanear()
            }
            Button{
                id: loopButton
                text: "Loop"
                checkable: true
                height: parent.height
                font.pixelSize: 15
                font.bold: checked
                onClicked: reproductor.bucle = checked
            }
        }

        Button{
            id: settingsButton
            text: swipeView.visible? "Settings" : "Volver"
            height: parent.height-10
            font.pixelSize: 15
            anchors{
                right: parent.right
                rightMargin: 6
                verticalCenter: parent.verticalCenter
            }
            onClicked: swipeView.visible? stack.push("Settings.qml") : stack.pop()
        }
        Rectangle{
            id: progressIndicator
            height: 4
            width: reproductor.progreso*parent.width
            color: "#00f1ff"
            anchors{
                top: parent.bottom
                left: parent.left
            }
        }
    }

    Reproductor{
        id: reproductor
        onFuenteChanged: console.log("Nueva fuente: "+fuente)
        almacenamiento: gestorSonidos.almacenamiento
        onAlmacenamientoChanged: console.log("Reproductor. Nuevo almacenamiento: "+almacenamiento)
    }

    Sonidos{
        id: sonidosListModel
    }

    GestorSonidos{
        id: gestorSonidos
        sonidosListModel: sonidosListModel
        onAlmacenamientoChanged: console.log("GestorSonidos. Nuevo almacenamiento: "+almacenamiento)
    }

    StackView{
        id: stack
        initialItem: swipeView
        anchors.fill: parent
    }

    SwipeView {
        id: swipeView
//        anchors.fill: parent
//        currentIndex: tabBar.currentIndex

        Page1 {
        }

//        Page2 {
//        }
    }

//    footer: TabBar {
//        id: tabBar
//        currentIndex: swipeView.currentIndex

//        TabButton {
//            text: qsTr("Sonidos 1")
//        }
//        TabButton {
//            text: qsTr("Sonidos 2")
//        }
//    }
    Component.onCompleted: gestorSonidos.escanear()
}
