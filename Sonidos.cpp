/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Sonidos.h"

Sonidos::Sonidos(QObject *parent){
	Q_UNUSED(parent);
}

QHash<int, QByteArray> Sonidos::roleNames() const{
	QHash<int, QByteArray> roles;
	roles[FuenteAudio] = "fuenteAudio";
	roles[CoverAudio] = "coverAudio";
	roles[EtiquetaAudio] = "etiquetaAudio";
	return roles;
}

void Sonidos::addElement(Fuente *newElement){
	emit beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_elements.append(newElement);
	emit endInsertRows();
}

void Sonidos::resetListModel(){
	emit beginResetModel();
	qDeleteAll(m_elements);
	m_elements.clear();
	emit endResetModel();
}

bool Sonidos::setData(const QModelIndex &index, const QVariant &value, int role){
	if(index.row() < 0 || index.row() >= m_elements.size() || !index.isValid())
		return false;
	switch (role) {
	case Qt::UserRole + 0:
		m_elements.at(index.row())->setFuenteAudio(value.toString());
		break;
	case Qt::UserRole + 1:
		m_elements.at(index.row())->setCoverAudio(value.toString());
		break;
	case Qt::UserRole + 2:
		m_elements.at(index.row())->setEtiquetaAudio(value.toString());
		break;
	default:
		return false;
	}
	return false;
}

Qt::ItemFlags Sonidos::flags(const QModelIndex &index) const{
	return Qt::ItemIsEditable | QAbstractListModel::flags(index);
}

int Sonidos::rowCount(const QModelIndex &parent) const{
	Q_UNUSED(parent);
	return m_elements.size();
}

QVariant Sonidos::data(const QModelIndex &index, int role) const{
	if(index.row() < 0 || index.row() >= m_elements.size() || !index.isValid())
	return QVariant();
	switch (role) {
	case Qt::UserRole + 0:
		return m_elements.at(index.row())->fuenteAudio();
	case Qt::UserRole + 1:
		return m_elements.at(index.row())->coverAudio();
	case Qt::UserRole + 2:
		return m_elements.at(index.row())->etiquetaAudio();
	default:
		return QVariant();
	}
	return false;
}
