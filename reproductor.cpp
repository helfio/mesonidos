/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "reproductor.h"

Reproductor::Reproductor(QObject *parent) : QObject(parent){
//  connect(this, SIGNAL(fuenteChanged(QString)), this, SLOT(reproducir()));
//  connect(&m_reproductor, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));
  connect(&m_reproductor, SIGNAL(positionChanged(qint64)), this, SLOT(setProgreso(qint64)));
  connect(&m_reproductor, SIGNAL(durationChanged(qint64)), this, SLOT(setDuracion(qint64)));
  connect(&m_reproductor, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(cambioDeEstado(QMediaPlayer::State)));
  m_reproductor.setNotifyInterval(30);
}

QString Reproductor::fuente() const{
  return m_fuente;
}

int Reproductor::volumen() const{
  return m_volumen;
}

qreal Reproductor::progreso() const{
  return m_progreso;
}

qint64 Reproductor::duracion() const{
  return m_duracion;
}

QString Reproductor::almacenamiento() const{
  return m_almacenamiento.absolutePath();
}

bool Reproductor::bucle() const{
  return m_bucle;
}

bool Reproductor::reproduciendo() const{
  return m_reproduciendo;
}

void Reproductor::setFuente(QString fuente){
  if (m_fuente == fuente)
    return;

  m_fuente = fuente;
  m_playlist.clear();
  m_playlist.addMedia(QUrl::fromLocalFile(m_almacenamiento.absoluteFilePath(fuente)));
  m_reproductor.setPlaylist(&m_playlist);
  emit fuenteChanged(m_fuente);
}

void Reproductor::setVolumen(int volumen){
  if (m_volumen == volumen)
    return;

  m_volumen = volumen;
  m_reproductor.setVolume(volumen);
  emit volumenChanged(m_volumen);
}

void Reproductor::reproducir(){
  qDebug() << "Reproduciendo";
  m_reproductor.stop();
  m_reproductor.play();
}

void Reproductor::stop(){
  m_reproductor.stop();
}

void Reproductor::setAlmacenamiento(QString almacenamiento){
  if (m_almacenamiento == almacenamiento)
    return;

  m_almacenamiento = QDir(almacenamiento);
  emit almacenamientoChanged(m_almacenamiento);
}

void Reproductor::setBucle(bool bucle){
  if (m_bucle == bucle)
    return;

  m_bucle = bucle;
  if (m_bucle){
      m_playlist.setPlaybackMode(QMediaPlaylist::Loop);
    }
  else{
      m_playlist.setPlaybackMode(QMediaPlaylist::Sequential);
    }
  emit bucleChanged(m_bucle);
}

void Reproductor::setReproduciendo(bool reproduciendo){
  if (m_reproduciendo == reproduciendo)
    return;

  m_reproduciendo = reproduciendo;
  emit reproduciendoChanged(m_reproduciendo);
}

void Reproductor::setProgreso(const qint64 &progreso){
  m_progreso = qreal(progreso)/qreal(m_duracion);
  emit progresoChanged(m_progreso);
}

void Reproductor::setDuracion(const qint64 &duracion){
  m_duracion = duracion;
  emit duracionChanged(m_duracion);
}

void Reproductor::cambioDeEstado(const QMediaPlayer::State &state){
  qDebug() << "State: " << state;
  if (state == QMediaPlayer::State::StoppedState){
      qDebug() << "Stop";
      setReproduciendo(false);
    emit fin();
    }
  else if (state == QMediaPlayer::State::PlayingState){
      qDebug() << "Playing";
      setReproduciendo(true);
    }
  else{
      qDebug() << "Pausa";
      setReproduciendo(false);
    }
}
