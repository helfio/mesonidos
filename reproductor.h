/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef REPRODUCTOR_H
#define REPRODUCTOR_H

#include <QObject>
#include <QString>
#include <QMediaPlayer>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include <QMediaPlaylist>

class Reproductor : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString fuente READ fuente WRITE setFuente NOTIFY fuenteChanged);
  Q_PROPERTY(int volumen READ volumen WRITE setVolumen NOTIFY volumenChanged);
  Q_PROPERTY(qreal progreso READ progreso NOTIFY progresoChanged);
  Q_PROPERTY(qint64 duracion READ duracion NOTIFY duracionChanged);
  Q_PROPERTY(QString almacenamiento READ almacenamiento WRITE setAlmacenamiento NOTIFY almacenamientoChanged);
  Q_PROPERTY(bool bucle READ bucle WRITE setBucle NOTIFY bucleChanged);
  Q_PROPERTY(bool reproduciendo READ reproduciendo WRITE setReproduciendo NOTIFY reproduciendoChanged);

public:
  explicit Reproductor(QObject *parent = nullptr);

  QString fuente() const;
  int volumen() const;
  qreal progreso() const;
  qint64 duracion() const;
  QString almacenamiento() const;

  bool bucle() const;

  bool reproduciendo() const;

public slots:
  void setFuente(QString fuente);
  void setVolumen(int volumen);
  void reproducir();
  void stop();
  void setAlmacenamiento(QString almacenamiento);
  void setBucle(bool bucle);

  void setReproduciendo(bool reproduciendo);

private slots:
  void setProgreso(const qint64 &progreso);
  void setDuracion(const qint64 &duracion);
  void cambioDeEstado(const QMediaPlayer::State &state);

private:
  QString m_fuente;
  QMediaPlayer m_reproductor;
  int m_volumen;
  qreal m_progreso;
  qint64 m_duracion;
  QDir m_almacenamiento;
  bool m_bucle = false;
  QMediaPlaylist m_playlist;

  bool m_reproduciendo;

signals:

void fuenteChanged(QString fuente);
void volumenChanged(int volumen);
void progresoChanged(qint64 progreso);
void duracionChanged(qint64 duracion);
void almacenamientoChanged(QDir almacenamiento);
void bucleChanged(bool bucle);
void fin();
void reproduciendoChanged(bool reproduciendo);
};

#endif // REPRODUCTOR_H
