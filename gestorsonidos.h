/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GESTORSONIDOS_H
#define GESTORSONIDOS_H

#include <QObject>
#include <QDir>
#include <QDebug>
#include <QStandardPaths>

#include "Sonidos.h"

class GestorSonidos : public QObject
{
  Q_OBJECT
  Q_PROPERTY(Sonidos* sonidosListModel READ sonidosListModel WRITE setSonidosListModel NOTIFY sonidosListModelChanged)
  Q_PROPERTY(QString almacenamiento READ almacenamiento NOTIFY almacenamientoChanged)

public:
  explicit GestorSonidos(QObject *parent = nullptr);

  Sonidos *sonidosListModel();
  QString almacenamiento() const;

public slots:
  Q_INVOKABLE void escanear();

  void setSonidosListModel(Sonidos* sonidosListModel);

private:
  QDir m_directorio;
  QStringList m_extensiones;
  Sonidos* m_sonidosListModel;

  QString m_almacenamiento;

signals:
void sonidosListModelChanged(Sonidos* sonidosListModel);
void almacenamientoChanged(QString almacenamiento);
};

#endif // GESTORSONIDOS_H
