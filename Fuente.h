/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FUENTE_H
#define FUENTE_H

#include <QObject>

class Fuente : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString fuenteAudio READ fuenteAudio WRITE setFuenteAudio NOTIFY fuenteAudioChanged)
	Q_PROPERTY(QString coverAudio READ coverAudio WRITE setCoverAudio NOTIFY coverAudioChanged)
	Q_PROPERTY(QString etiquetaAudio READ etiquetaAudio WRITE setEtiquetaAudio NOTIFY etiquetaAudioChanged)

public:
	explicit Fuente(QObject *parent = nullptr);

	QString fuenteAudio() const;
	void setFuenteAudio(const QString &fuenteAudio);

	QString coverAudio() const;
	void setCoverAudio(const QString &coverAudio);

	QString etiquetaAudio() const;
	void setEtiquetaAudio(const QString &etiquetaAudio);


signals:
	void fuenteAudioChanged();
	void coverAudioChanged();
	void etiquetaAudioChanged();

private:
	QString m_fuenteAudio;
	QString m_coverAudio;
	QString m_etiquetaAudio;

};

#endif // FUENTE_H
