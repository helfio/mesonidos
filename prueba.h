/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PRUEBA_H
#define PRUEBA_H

#include <QAudioSystemPlugin>

// Fuentes de informacion
// https://stackoverflow.com/questions/41977236/how-to-build-in-a-static-qt-plugin-into-an-application-with-cmake
// https://doc.qt.io/qt-5/qaudiosystemplugin.html#createInput
// https://doc.qt.io/qt-5/plugins-howto.html

class prueba : public QAudioSystemPlugin
{

  Q_PLUGIN_METADATA(IID "org.qt-project.qt.audiosystemfactory/5.0" FILE "virtual.json")

public:
  prueba();
};

#endif // PRUEBA_H
