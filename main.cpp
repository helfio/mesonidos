/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QGuiApplication>
#include <QQmlApplicationEngine>
//#include <QtPlugin>

#include "reproductor.h"
#include "Sonidos.h"
#include "gestorsonidos.h"

//Q_IMPORT_PLUGIN(prueba)

//Info para varios microfonos en linux
//https://askubuntu.com/questions/868817/collecting-and-mixing-sound-input-from-different-microphones

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setOrganizationDomain("jpns");
    app.setApplicationDisplayName("Mesonidos");
    app.setApplicationVersion("0.1");
    app.setApplicationName("mesonidos");

    qmlRegisterType<Reproductor>("jpns.reproductor", 1, 0, "Reproductor");
    qmlRegisterType<Sonidos>("jpns.reproductor", 1, 0, "Sonidos");
    qmlRegisterType<GestorSonidos>("jpns.reproductor", 1, 0, "GestorSonidos");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,&app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
