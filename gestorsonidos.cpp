/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "gestorsonidos.h"

GestorSonidos::GestorSonidos(QObject *parent) : QObject(parent){
  m_extensiones << "*.ogg" << "*.mp3";
//  this->escanear();
}

Sonidos *GestorSonidos::sonidosListModel(){
  return m_sonidosListModel;
}

QString GestorSonidos::almacenamiento() const{
  return m_directorio.path();
}

void GestorSonidos::escanear(){
  m_directorio = *new QDir("sonidos/");
#if defined(Q_OS_ANDROID)
  m_directorio = *new QDir(QStandardPaths::standardLocations(QStandardPaths::DataLocation)[1]);
//  qDebug() << "Directorio de Android" << endl;
#endif
  emit almacenamientoChanged(m_directorio.absolutePath());
  // Regular Linux
  m_sonidosListModel->resetListModel();
  QStringList lista_sonidos = m_directorio.entryList(m_extensiones);
  for (int i=0; i < lista_sonidos.length(); i++){
      Fuente * fuente = new Fuente();
      fuente->setFuenteAudio(m_directorio.absolutePath()+"/"+lista_sonidos.at(i));
//      qDebug() << m_directorio.absolutePath()+"/"+lista_sonidos.at(i);
      fuente->setCoverAudio("");
      QString nombre = lista_sonidos.at(i).section(".",0,-2);
      fuente->setEtiquetaAudio(nombre);
      m_sonidosListModel->addElement(fuente);
    }
  //  qDebug() << m_sonidosListModel->data(QModelIndex(),1);
}

void GestorSonidos::setSonidosListModel(Sonidos *sonidosListModel){
  m_sonidosListModel = sonidosListModel;
  emit sonidosListModelChanged(m_sonidosListModel);
}
