/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Fuente.h"

Fuente::Fuente(QObject *parent) : QObject(parent){
Q_UNUSED(parent);
}

QString Fuente::fuenteAudio() const{
	return m_fuenteAudio;
}

void Fuente::setFuenteAudio(const QString &fuenteAudio){
	if (m_fuenteAudio == fuenteAudio)
		return;
	m_fuenteAudio = fuenteAudio;
	emit fuenteAudioChanged();
}

QString Fuente::coverAudio() const{
	return m_coverAudio;
}

void Fuente::setCoverAudio(const QString &coverAudio){
	if (m_coverAudio == coverAudio)
		return;
	m_coverAudio = coverAudio;
	emit coverAudioChanged();
}

QString Fuente::etiquetaAudio() const{
	return m_etiquetaAudio;
}

void Fuente::setEtiquetaAudio(const QString &etiquetaAudio){
	if (m_etiquetaAudio == etiquetaAudio)
		return;
	m_etiquetaAudio = etiquetaAudio;
	emit etiquetaAudioChanged();
}

