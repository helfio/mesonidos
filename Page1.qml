/*
Copyright (C) 2020  Pablo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Controls.Styles 1.4

Page {
    width: 600
    height: 400
    background: Rectangle{
        anchors.fill: parent
        color: "#00bbff"
    }

//    header: Label {
//        id: headerLabel
//        text: qsTr("Sonidos 1")
//        font.pixelSize: Qt.application.font.pixelSize * 2
//        padding: 10
//    }

    Label{
        id: instructions
        anchors.centerIn: parent
        width: 0.9*parent.width
//        visible: sonidosListModel.rowCount() === 0
        visible: mainView.count === 0
        text: "Hi!\nPlease, copy your sounds into the following folder:\n"+gestorSonidos.almacenamiento+"\nThen press "+scanButton.text
    }

    GridView{
        id: mainView
        clip: true
        model: sonidosListModel
        anchors.fill: parent
        delegate: Item {
            id: buttonContainer
            height: 100
            width: 100
            Rectangle{
                color: "transparent"
                border.color: "green"
                anchors.fill: parent
                visible: dev
            }
            Rectangle{
                height: 60
                width: height
                color: "transparent"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: 10
                }
                RoundButton{
                    id: soundButton
                    height: parent.height-4
                    width: height
                    radius: 10
                    text: "🎵"
                    font.pixelSize: 30
                    anchors.centerIn: parent
                    background: Rectangle {
                        implicitWidth: 100
                        implicitHeight: 40
                        color: parent.down ? "#d6d6d6" : "#f6f6f6"
                        radius: parent.radius
                    }
                    onClicked: {
                        reproductor.fuente = fuenteAudio
                        reproductor.reproducir()
                    }
                }
            }
            Label{
                id: soundLabel
                width: parent.width*0.9
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideRight
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.bottom
                    bottomMargin: 5
                }
                text: etiquetaAudio
            }
        }
    }

}
